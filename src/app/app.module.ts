import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AgmCoreModule } from '@agm/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TrackVesselService } from './services/track-vessel.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCJm5m3yx6H-vYuGbDwhK5DBAargAlmHHM'
    }),
    HttpClientModule,
    NgbModule,
    AppRoutingModule
  ],
  providers: [TrackVesselService],
  bootstrap: [AppComponent]
})
export class AppModule { }
