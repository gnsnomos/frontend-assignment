import { Component, OnDestroy, OnInit } from '@angular/core';
import { TrackVesselService } from './services/track-vessel.service';
import { Subscription } from 'rxjs';
import { Title } from '@angular/platform-browser';

export interface Position {
  MMSI: string;
  LAT: string | number;
  LON: string | number;
  SPEED: string;
  HEADING: string;
  COURSE: string;
  STATUS: string;
  TIMESTAMP: string;
  SHIP_ID: string;
}

@Component({
  selector   : 'app-root',
  templateUrl: './app.component.html',
  styleUrls  : ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  public readonly Periods = ['daily', 'hourly'];
  public lat = 0;
  public lng = 0;
  public mmsi = 241486000;
  public days = 20;
  public daysMax = 190;
  public daysMin = 1;
  public daysNumberWrong = false;
  public period: 'daily' | 'hourly' = 'daily';
  public positionHistory: Position[] = [];

  private _subscription: Subscription;

  constructor (private _trackVesselService: TrackVesselService, private titleService: Title) { }

  public ngOnInit (): void {
    this._requestPosition();
  }

  public ngOnDestroy (): void {
    this._reset();
  }

  public newMmsi (value: string): void {
    if (value !== '' && Number.parseFloat(value) !== this.mmsi && !this.daysNumberWrong) {
      this._reset();
      this.mmsi = Number.parseFloat(value);
      this._requestPosition();
    }
  }

  public newDays (value: string): void {
    const days = Number.parseInt(value);

    if (days < this.daysMin || days > this.daysMax) {
      this.daysNumberWrong = true;
      return;
    }
    this.daysNumberWrong = false;

    if (days !== this.days) {
      this._reset();
      this.days = days;
      this._requestPosition();
    }
  }

  public newPeriod (value: 'daily' | 'hourly'): void {
    if (value !== this.period && !this.daysNumberWrong) {
      this._reset();
      this.period = value;
      this._requestPosition();
    }
  }

  public prettyDate (value: string): string {
    const date = new Date(value);
    return this._twoDigits(date.getHours()) + ':'
      + this._twoDigits(date.getMinutes()) + ' '
      + this._twoDigits(date.getDate()) + '/'
      + this._twoDigits((date.getMonth() + 1)) + '/'
      + date.getFullYear();
  }

  private _twoDigits (value: number): string {
    return value.toString().padStart(2, '0');
  }

  private _requestPosition (): void {
    this.titleService.setTitle('Daily track for: ' + this.mmsi);

    this._subscription = this._trackVesselService.getPositionByMMSI(
      this.mmsi,
      this.period,
      this.days
    ).subscribe((positions: Position[]) => {
      console.log(positions);
      if (positions.length > 0) {
        positions.forEach(position => {
          position.LAT = Number.parseFloat(<string> position.LAT);
          position.LON = Number.parseFloat(<string> position.LON);

          this.positionHistory.push(position);
        });

        this.lat = <number> this.positionHistory[positions.length - 1].LAT;
        this.lng = <number> this.positionHistory[positions.length - 1].LON;
      }
    });
  }

  private _reset (): void {
    this._subscription.unsubscribe();
    this.positionHistory = [];
    this.daysNumberWrong = false;
  }
}
