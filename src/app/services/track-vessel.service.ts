import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class TrackVesselService {
  private _endpointUrl = 'https://services.marinetraffic.com/api/exportvesseltrack/';
  private _apiKey = 'cf8f05df0b57bfae43e762cc61fd381239c4c042';
  private _counter = 0;

  constructor (private _http: HttpClient) {
    this._counter = 0;
  }

  public getPositionByMMSI (mmsi, period: 'daily' | 'hourly' = 'daily', days = 10) {
    return this._http.get(
      this._endpointUrl + this._apiKey + '/v:2/period:' + period + '/days:' + days + '/mmsi:' + mmsi + '/protocol:jsono'
      );
  }
}
